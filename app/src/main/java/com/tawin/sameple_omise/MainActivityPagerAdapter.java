package com.tawin.sameple_omise;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tawin.sameple_omise.fragment.customer.CustomerFragment;
import com.tawin.sameple_omise.fragment.transaction.TransactionFragment;

/**
 * Created by tawin on 8/12/2015 AD.
 */
public class MainActivityPagerAdapter extends FragmentPagerAdapter {

    private final String[] TITLES = {"Customer", "Transaction"};

    public MainActivityPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        if (TITLES[position].equalsIgnoreCase("Customer")) {
            return CustomerFragment.newInstance();
        } else if (TITLES[position].equalsIgnoreCase("Transaction")) {
            return TransactionFragment.newInstance();
        } else {
            return CustomerFragment.newInstance();
        }
    }

}
