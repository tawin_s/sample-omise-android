package com.tawin.sameple_omise.fragment.customer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tawin.sameple_omise.R;

import java.util.ArrayList;

/**
 * Created by tawin on 8/12/2015 AD.
 */
public class ListViewCustomerAdapter extends BaseAdapter {
    private static ArrayList<String> listContact;

    private LayoutInflater mInflater;

    public ListViewCustomerAdapter(Context photosFragment, ArrayList<String> results){
        listContact = results;
        mInflater = LayoutInflater.from(photosFragment);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listContact.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return listContact.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;
        if(convertView == null){
            convertView = mInflater.inflate(R.layout.customer_item, null);
            holder = new ViewHolder();
            holder.textName = (TextView) convertView.findViewById(R.id.customer_item_name);
            holder.textEmail = (TextView) convertView.findViewById(R.id.customer_item_email);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textName.setText(listContact.get(position));
        holder.textEmail.setText("tawin.sup@email.com");

        return convertView;
    }

    static class ViewHolder{
        TextView textName, textEmail;
    }
} 
