package com.tawin.sameple_omise.fragment.customer;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.tawin.sameple_omise.R;

import java.util.ArrayList;

public class CustomerFragment extends Fragment {

    private static final int INITIAL_DELAY_MILLIS = 2000;

    public static CustomerFragment newInstance() {
        return  new CustomerFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.customer_fragment, container, false);
        ArrayList<String> customers = getCustomers();
        ListView listView = (ListView)rootView.findViewById(R.id.customer_list_view);
        ListViewCustomerAdapter listViewCustomerAdapter = new ListViewCustomerAdapter(getActivity(), customers);
        SwingBottomInAnimationAdapter swingBottomInAnimationAdapter = new SwingBottomInAnimationAdapter(listViewCustomerAdapter);

        swingBottomInAnimationAdapter.setAbsListView(listView);
        assert swingBottomInAnimationAdapter.getViewAnimator() != null;
        swingBottomInAnimationAdapter.getViewAnimator().setInitialDelayMillis(INITIAL_DELAY_MILLIS);
        listView.setAdapter(new ListViewCustomerAdapter(getActivity(), customers));
        return rootView;
    }
    private ArrayList<String> getCustomers(){
        ArrayList<String> contactList = new ArrayList<>();

        contactList.add("Tawin Supayanant");
        contactList.add("Mai Mai");
        contactList.add("Love you");

        return contactList;
    }
}

