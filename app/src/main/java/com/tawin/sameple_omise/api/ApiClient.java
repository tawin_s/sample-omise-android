package com.tawin.sameple_omise.api;

import com.tawin.sameple_omise.model.Customer;

import java.util.List;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.Callback;

public class ApiClient {
    private static SampleOmiseApiInterface sampleOmiseApiInterface;

    public static SampleOmiseApiInterface getSampleOmiseApiClient() {
        if (sampleOmiseApiInterface == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://sample-omise.herokuapp.com/v1")
                    .build();

            sampleOmiseApiInterface = restAdapter.create(SampleOmiseApiInterface.class);
        }

        return sampleOmiseApiInterface;
    }

    public interface SampleOmiseApiInterface {
        @GET("/v1/customers?access_token=3a787608d770ed7129021ef596fcafb216e35b15ba0ab339d6a70b813e8da2bb")
        void getStreams(Callback<Customer> callback);
    }
}
